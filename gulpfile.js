var gulp    = require( 'gulp' );
var replace = require( 'gulp-replace' );
var rename  = require( 'gulp-rename' );
var del     = require( 'del' );
var shell   = require( 'gulp-shell' );

var phpVersions = [
	'7.3',
	'7.2',
	'7.1',
	'7.0',
	'5.6',
];

var imageName = 'alexsanford1/wordpress-dev';
var imageTag  = ( phpVersion ) => `php${phpVersion}`;


// Utility tasks.

gulp.task( 'clean', function() {
	return del( [ 'dockerfiles/' ] );
} );


// Build dockerfiles.

phpVersions.forEach( ( phpVersion ) => {
	gulp.task( `dockerfile:${phpVersion}`, function() {
		return gulp.src( [ 'Dockerfile.template' ] )
			.pipe( replace( '%%PHP_VERSION%%', phpVersion ) )
			.pipe( rename( `Dockerfile.php-${phpVersion}` ) )
			.pipe( gulp.dest( 'dockerfiles/' ) );
	} )
} );

gulp.task( 'dockerfile:all', [ 'clean' ].concat(
	phpVersions.map(
		( version ) => `dockerfile:${version}`
	)
) );


// Build docker images.

phpVersions.forEach( ( phpVersion ) => {
	gulp.task( `image:${phpVersion}`, [ `dockerfile:${phpVersion}` ],
		shell.task(
			`docker build . -f dockerfiles/Dockerfile.php-${phpVersion} -t ${imageName}:${imageTag( phpVersion )}`
		)
	);
} );

gulp.task( 'image:all', phpVersions.map(
	( version ) => `image:${version}`
) );


// Push docker images.

phpVersions.forEach( ( phpVersion ) => {
	gulp.task( `push:${phpVersion}`, [ `image:${phpVersion}` ],
		shell.task(
			`docker push ${imageName}:${imageTag( phpVersion )}`
		)
	);
} );

gulp.task( 'push:all', phpVersions.map(
	( version ) => `push:${version}`
) );


// Pull latest WordPress images.

gulp.task( 'update-wp', shell.task(
	"docker images | grep '^wordpress' | grep -v '<none>' | awk '{print $1 \":\" $2}' | xargs -L1 docker pull"
) );


// Default task.

gulp.task( 'default', [ 'push:all' ] );
